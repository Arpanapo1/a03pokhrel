function calculate() {
    console.log("Inside calculate method");
    var numOfDays = parseInt(document.getElementById("numOfDays").value);
    var rentPerDay = parseFloat(document.getElementById("rentPerDay").value);
    var tax = parseFloat(document.getElementById("tax").value);

    var total = calcTotal(numOfDays, rentPerDay, tax);

    if (total > 0) {
        document.getElementById('totalLbl').innerHTML = '$' + numOfDays * rentPerDay;
        document.getElementById('taxLbl').innerHTML = '$' + (numOfDays * rentPerDay) * tax/100;
        document.getElementById('grandTotalLbl').innerHTML = '$' + total;
    }

    console.log(total);

}

function calcTotal(numOfDays, rentPerDay, tax) {
    if (isNaN(numOfDays) || isNaN(rentPerDay) || isNaN(tax)) {
        window.alert('The given argument is not a number');
        throw "One argument is missing"
    }
    if (numOfDays < 0 || rentPerDay < 0 || tax < 0) {
        window.alert('Enter positive number');
        throw "The given argument is negative"
    }
    var total = 0.0;
    if (numOfDays > 0 && rentPerDay > 0 && tax > 0) {
        total = (numOfDays * rentPerDay) + ((numOfDays * rentPerDay) * tax/100);
    }
    return total; 
}

function reset() {
    document.getElementById('totalLbl').innerHTML = '';
    document.getElementById('taxLbl').innerHTML = '';
    document.getElementById('grandTotalLbl').innerHTML = '';
    $("#numOfDays").val("");
    $("#rentPerDay").val("");
    $("#tax").val("");
}